<a name="readme-top"></a>



<br />
<div align="center">
<!-- Project README or deployed webpage -->
  <a href="https://gitlab.com/waynebasile/readme-template/-/blob/main/README.md?ref_type=heads">
    <!-- Bootstrap SVG icon from https://icons.getbootstrap.com/ -->
    <img src="icon.svg" alt="Logo" width="100" height="100">
  </a>

<h3 align="center">Project Title</h3>

  <p align="center">
    Project introduction. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet. Cursus turpis massa tincidunt dui ut ornare lectus sit.
    <br />
    <br />
    <!-- Project repository or webpage link -->
    <a href="https://gitlab.com/waynebasile/readme-template"><strong>View Project »</strong></a>
    <!-- <a href="https://<user_or_group_name>.gitlab.io/<project_name>/"><strong>View Webpage »</strong></a> -->
    <br />
    <br />
    <a href="https://gitlab.com/waynebasile/readme-template/-/blob/main/README.md?ref_type=heads"><strong>Explore the Docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/waynebasile/readme-template/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/waynebasile/readme-template/-/issues">Request Feature</a>
  </p>
</div>



<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
      <li><a href="#design">Design</a></li>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#local-installation">Installation</a></li>
        <li><a href="#deploy-frontend">Deploy Frontend</a></li>
        <li><a href="#deploy-application">Deploy Application</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#contributors">Contributors</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
    <li><a href="#project-status">Project Status</a></li>
  </ol>
</details>



## About The Project

[![Project Name Screen Shot][project-screenshot]](https://gitlab.com/waynebasile/readme-template)

Health and Swollenness is a fitness application that allows users to create and view custom workout routines to achieve their fitness goals. It was developed collaboratively using FastAPI, PostgreSQL, front-end/back-end authentication, Vite, React, and Bootstrap.

### Design

[Excalidraw Wireframes](https://excalidraw.com/#json=hiIstjnZBxnFIvdI5pLJo,oEfsrhHNlrHvw-4dMJimwA) · [Excalidraw API Design](https://excalidraw.com/#json=tT9BzEapAOCLdds0O30BU,Vus2g_CyOYrmnN90PNWDrA)

### Built With

[![FastAPI][Fastapi.tiangolo.com]][Fastapi-url] &nbsp; [![React][React.js]][React-url] &nbsp; [![ReactRouter][ReactRouter.com]][ReactRouter-url]

[![Docker][Docker.com]][Docker-url] &nbsp; [![Bootstrap][Bootstrap.com]][Bootstrap-url] &nbsp; [![HTML5][HTML5.com]][HTML5-url] &nbsp; [![CSS3][CSS3.com]][CSS3-url]

[![Python][Python.org]][Python-url] &nbsp; [![Javascript][Javascript.com]][Javascript-url] &nbsp; [![PostgreSQL][PostgreSQL.org]][PostgreSQL-url]



## Getting Started

### Prerequisites

The installation instructions assume your system has the following software: [Google Chrome](https://www.google.com/chrome/) and [Docker](https://www.docker.com/).

If you don't have these (or equivalent) software, please install them before proceeding.

To get a local copy of Health and Swolleness up and running on your machine follow these simple steps.

### Local Installation

1. Fork and clone the [repository](https://gitlab.com/WayneBasile/health-and-swollenness/-/forks/new)

2. In terminal, run `cd <repository_directory>`

3. Rename the .env.sample file to .env

4. Remove the .gitlab-ci.yml file

5. In terminal, run `docker volume create pg-admin`

6. In terminal, run `docker volume create fastapi-data`

7. In terminal, run `docker compose build`

8. In terminal, run `docker compose up`

9. Navigate to [localhost:3000](http://localhost:3000/)

### Deploy Frontend

0. Complete Local Installation instructions

1. Add .gitlab-ci.yml file

2. Uncomment remote frontend VITE_PUBLIC_URL in .env

3. Uncomment local backend VITE_REACT_API_HOST in .gitlab-ci.yml

4. Add CI/CD VITE_PUBLIC_URL variable

5. Update VITE_REACT_API_HOST variable to http://localhost:8000

6. Push changes to repository

### Deploy Application

0. Complete Deploy Frontend instructions

1. Uncomment remote backend VITE_REACT_API_HOST in .gitlab-ci.yml

2. Add CI/CD SIGNING_KEY and DATABASE_URL variables

3. Update CI/CD VITE_REACT_API_HOST variable to cloud provider URL

4. Push changes to repository

5. Deploy backend with cloud provider



## Roadmap

- [ ] Add workout journal
- [x] Switch to Vite + React
- [ ] Use Redux Toolkit
- [ ] Add websocket group chat
- [ ] Add third part API for estimated calories burned

See the [open issues](https://gitlab.com/WayneBasile/health-and-swollenness/-/issues) for a full list of proposed features (and known issues).



## Contact

[![Contributors][wayne-shield]][wayne-url]



## Contributors

[Wayne Basile](https://www.linkedin.com/in/waynebasile/) · [Zach Quail](https://www.linkedin.com/in/zach-quail-2a7b8585/)

[Mohammad Rahman](https://www.linkedin.com/in/marahman4748/) · [Tom Perry](https://www.linkedin.com/in/tomperry718/)



## Acknowledgments

[FastAPI](https://fastapi.tiangolo.com/) · [React](https://react.dev/) · [React Router](https://reactrouter.com/en/main)

[Docker](https://www.docker.com/) · [Bootstrap](https://getbootstrap.com/) · [HTML5](https://developer.mozilla.org/en-US/docs/Web/HTML) · [CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS)

[Python](https://www.python.org/) · [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) · [PostgreSQL](https://www.postgresql.org/)

[Shields.io](https://shields.io/) · [Simple Icons](https://simpleicons.org/) · [Pexels](https://www.pexels.com/) · [Excalidraw](https://excalidraw.com/)

<p align="right">(<a href="#readme-top">back to top</a>)</p>



[project-screenshot]: ghi/public/screenshot.png

[Fastapi.tiangolo.com]: https://img.shields.io/badge/Fastapi-009688?style=for-the-badge&logo=fastapi&logoColor=white
[FastAPI-url]: https://fastapi.tiangolo.com/

[React.js]: https://img.shields.io/badge/React-61DAFB?style=for-the-badge&logo=react&logoColor=white
[React-url]: https://reactjs.org/

[Bootstrap.com]: https://img.shields.io/badge/Bootstrap-7952B3?style=for-the-badge&logo=bootstrap&logoColor=white
[Bootstrap-url]: https://getbootstrap.com

[Docker.com]: https://img.shields.io/badge/Docker-2496ED?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://www.docker.com/

[HTML5.com]: https://img.shields.io/badge/HTML5-E34F26?style=for-the-badge&logo=html5&logoColor=white
[HTML5-url]: https://developer.mozilla.org/en-US/docs/Web/HTML

[Python.org]: https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white
[Python-url]: https://www.python.org/

[Javascript.com]: https://img.shields.io/badge/JavaScript-F7DF1E?style=for-the-badge&logo=javascript&logoColor=white
[Javascript-url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript

[PostgreSQL.org]: https://img.shields.io/badge/PostgreSQL-4169E1?style=for-the-badge&logo=postgresql&logoColor=white
[PostgreSQL-url]: https://www.postgresql.org/

[ReactRouter.com]: https://img.shields.io/badge/React_Router-CA4245?style=for-the-badge&logo=reactrouter&logoColor=white
[ReactRouter-url]: https://reactrouter.com/en/main

[CSS3.com]: https://img.shields.io/badge/CSS3-1572B6?style=for-the-badge&logo=css3&logoColor=white
[CSS3-url]: https://developer.mozilla.org/en-US/docs/Web/CSS

[wayne-shield]: https://img.shields.io/badge/Wayne_Basile-0A66C2?logo=linkedin&style=for-the-badge
[wayne-url]: https://www.linkedin.com/in/waynebasile/
